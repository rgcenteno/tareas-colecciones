/*  
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareascolecciones;

import java.util.Collection;
/**
 *
 * @author rgcenteno
 */
public class TareasColecciones {
    
    private static Collection<String> c;
    private static java.util.Scanner teclado;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Inicializamos objetos
        c = new java.util.ArrayList<>();
        teclado = new java.util.Scanner(System.in);
        
        rellenarCollection();
        
        String opcion = "";
        do{
            opcion = showMenuPrincipal();
            String nombre;
            switch(opcion){
                case "1":
                    System.out.println("Inserte el nombre que quiere comprobar:");
                    nombre = teclado.nextLine();
                    if(c.contains(nombre)){
                        System.out.println("El nombre existe en la colección");
                    }
                    else{
                        System.out.println("El nombre NO existe en la colección");
                    }
                    break;
                case "2":
                    System.out.println("Inserte el nombre que desea borrar.");
                    nombre = teclado.nextLine();
                    if(c.remove(nombre)){
                        System.out.println(c);
                    }
                    else{
                        System.out.println("El elemento no existe");
                    }
                    break;
                case "3":
                    System.out.println("Inserte la cadena que deben contener los elementos a borrar");
                    nombre = teclado.nextLine();
                    int n = eliminarStringContiene(nombre);
                    if(n == 0){
                        System.out.println("No se ha eliminado ningún elemento.");
                    }
                    else{
                        System.out.println("Se han eliminado " + n + " elementos");
                    }
                    //eliminarStringContiene2(nombre);
                    System.out.println(c);
                    break;
                case "4":
                    System.out.println(c);
                    break;
                default:
                    System.out.println("Opción inválida.");
                    break;
            }
        }while(!opcion.equals("0"));
    }
    
    private static void rellenarCollection(){
        String nombre;
        do{
            System.out.println("Por favor inserte un nombre. Escriba final para terminar.");
            nombre = teclado.nextLine();            
            if(!nombre.equals("final")){
                c.add(nombre);
            }
        }
        while(!nombre.equals("final"));        
        
    }
    
    private static int eliminarStringContiene(String s){
        java.util.Iterator<String> it = c.iterator();
        int eliminados = 0;
        while (it.hasNext()) {
            String next = it.next();
            if(next.contains(s)){
                it.remove();
                eliminados++;
            }            
        }
        return eliminados;
    }
    
    private static boolean eliminarStringContiene2(String s){
        return c.removeIf(elemento -> elemento.contains(s));
    }
    
    private static String showMenuPrincipal(){        
        System.out.println("********************************");
        System.out.println("* 1. Existe nombre             *");
        System.out.println("* 2. Borrar nombre             *");
        System.out.println("* 3. Borrar contiene           *");
        System.out.println("* 4. Mostrar coleccion         *");
        System.out.println("*                              *");
        System.out.println("* 0. Salir                     *");
        System.out.println("********************************");      
        return teclado.nextLine();
    }
    
    
    
}
