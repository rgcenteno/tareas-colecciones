/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareascolecciones;

import java.util.Collection;
/**
 *
 * @author rgcenteno
 */
public class TareaColecciones2 {
    private static Collection<Integer> notas;
    private static java.util.Scanner teclado;
    
    public static void main(String[] args) {
        teclado = new java.util.Scanner(System.in);
        notas = new java.util.ArrayList<>();
        int nota = -2;
        do{
            System.out.println("Inserte la siguiente nota");
            if(teclado.hasNextInt()){
                nota = teclado.nextInt();
                if(nota >= 0 && nota <= 10){
                    notas.add(nota);
                }
                else if(nota != -1){
                    System.out.println("La nota debe tener un valor comprendido entre 0 y 10");
                }
            }
            teclado.nextLine();
        }
        while(nota != -1);
        if(!notas.isEmpty()){
            System.out.println("Estado actual: ");
            System.out.println(notas);
            System.out.println("\nMedia: " + calcularMedia());
            System.out.println("Mínimo:" + minimo());
            System.out.println("Máximo:" + maximo());
            
            System.out.println("\nEliminadas " + eliminarSuspensos() + " notas");
            System.out.println(notas);
            System.out.println("\nMedia: " + calcularMedia());
            System.out.println("Mínimo:" + minimo());
            System.out.println("Máximo:" + maximo());
        }
        
    }
    
    private static float calcularMedia(){
        int suma = 0;        
        for(Integer i : notas){
            suma += i;
        }
        return suma/(float)notas.size();
    }
    
    private static int minimo(){
        int minimo = 11;
        for(int i : notas){
            if(minimo > i){
                minimo = i;
            }
        }
        return minimo;
    }
    
    private static int maximo(){
        int maximo = -1;
        for(int i : notas){
            if(maximo < i){
                maximo = i;
            }
        }
        return maximo;
    }
    
    private static int eliminarSuspensos(){
        //notas.removeIf(nota -> nota < 5);        
        int eliminados = 0;
        java.util.Iterator<Integer> it = notas.iterator();
        while (it.hasNext()) {
            Integer next = it.next();
            if(next < 5){
                it.remove();
                eliminados++;
            }
        }
        return eliminados;
    }
}
